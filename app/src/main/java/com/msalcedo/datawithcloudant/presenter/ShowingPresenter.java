package com.msalcedo.datawithcloudant.presenter;

import android.util.Log;

import com.msalcedo.datawithcloudant.model.ShowingModel;
import com.msalcedo.datawithcloudant.model.base.IShowingModel;
import com.msalcedo.datawithcloudant.view.base.IShowingView;

import java.util.List;

/**
 * Created by msalcedo on 09/02/15.
 */
public class ShowingPresenter implements IShowingModel.IShowingModelCallbacks{

    /* ***********************************************
     *************** CONSTANTS ***********************
     * ********************************************* */

    private static final String TAG = "TAG_ShowingPresenter";



    /* ***********************************************
     *************** FIELDS **************************
     * ********************************************* */

    /** Model */
    private IShowingModel mModel;

    /** View */
    private IShowingView mView;


    /* ***********************************************
     *************** METHODS *************************
     * ********************************************* */


    public ShowingPresenter(IShowingView mView) {
        this.mView = mView;
        // instance model
        mModel = new ShowingModel(mView.getContext());
        mModel.setCallbacks(this);
        Log.d(TAG, "Init");


    }


     public void getItems() {
        mView.startRefreshView();
        mModel.getItems();
    }

    public void setItem(String string){
        mModel.addItem(string);
    }


    @Override
    public void onItemsSuccess(List<String> items) {
        mView.setItems(items);
        mView.stopRefreshView();

    }

    @Override
    public void onItemsFailed() {
        mModel.getItems();
    }

    @Override
    public void onFailedConnection() {
        mView.showErrorConection();
        mView.stopRefreshView();

    }
}
