package com.msalcedo.datawithcloudant.view.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ListView;

import com.msalcedo.datawithcloudant.R;
import com.msalcedo.datawithcloudant.model.database.Task;
import com.msalcedo.datawithcloudant.view.base.IShowingView;
import com.msalcedo.datawithcloudant.view.helper.ListShowingAdapter;
import com.msalcedo.datawithcloudant.view.widget.MultiSwipeRefreshLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by msalcedo on 09/02/15.
 */
public class ShowingFragment extends Fragment
    implements
        SwipeRefreshLayout.OnRefreshListener,
        IShowingView,
        ListShowingAdapter.OnClickShowingListener {
    private static final String TAG = "TAG_ShowingFragment";


 /* ***********************************************
     *************** FIELDS **************************
     * ********************************************* */

    private ArrayList<String> list;




    //references to xml
    @InjectView(R.id.list_result)
    ListView mList;



    @InjectView(R.id.swipe_container)
    MultiSwipeRefreshLayout mSwipeLayout;



    private ListShowingAdapter mAdapter;



    /* ***********************************************
     *************** METHODS *************************
     * ********************************************* */


    public static ShowingFragment newInstance() {
        return new ShowingFragment();
    }

    public ShowingFragment() {
        // Required empty public constructor
        list = new ArrayList<String>();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_showing, container, false);
        ButterKnife.inject(this,root);

        // Instance presenter and sill data user
        init();
        Log.d(TAG, "Init");



        return root;
    }

    private void init() {
        mSwipeLayout.setOnRefreshListener(this);
        mSwipeLayout.setColorScheme(
                R.color.refresh_progress_1,
                R.color.refresh_progress_2,
                R.color.refresh_progress_3,
                R.color.refresh_progress_4);
    }


    @Override
    public void setItems(List<String> items) {
        /*if (list.isEmpty()){
            mAdapter = new ListShowingAdapter(getActivity(), new ArrayList<Task>(items),this);
        }
        else{
            mAdapter.set(getActivity(), new ArrayList<Task>(items));
        }
        list = new ArrayList<String>(items);
        mList.setAdapter(mAdapter);*/
    }

    @Override
    public void stopRefreshView() {
        mSwipeLayout.setRefreshing(false);

    }

    @Override
    public void startRefreshView() {
        mSwipeLayout.setRefreshing(true);

    }

    @Override
    public void showErrorConection() {
        Crouton.cancelAllCroutons();
        Crouton.makeText(getActivity(), R.string.error_internet, Style.ALERT).show();
    }

    @Override
    public void onRefresh() {


    }

    @Override
    public Context getContext() {
        return getActivity();

    }
}
