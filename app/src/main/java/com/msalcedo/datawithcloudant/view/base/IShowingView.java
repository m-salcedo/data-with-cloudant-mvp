package com.msalcedo.datawithcloudant.view.base;

import android.support.v4.app.Fragment;

import java.util.List;

/**
 * Created by msalcedo on 09/02/15.
 */
public interface IShowingView extends IBaseView {


        void setItems(List<String> items);

        void stopRefreshView();
        void startRefreshView();


        void showErrorConection();



}
