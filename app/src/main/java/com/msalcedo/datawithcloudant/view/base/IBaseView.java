package com.msalcedo.datawithcloudant.view.base;

import android.content.Context;

/**
 * Created by msalcedo on 09/02/15.
 */
public interface IBaseView {
    public Context getContext();
}