package com.msalcedo.datawithcloudant.view;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.msalcedo.datawithcloudant.R;
import com.msalcedo.datawithcloudant.view.fragment.ShowingFragment;

import butterknife.ButterKnife;


public class MainActivity extends FragmentActivity {
 /* ***********************************************
     *************** CONSTANTS ***********************
     * ********************************************* */

    private static final String TAG = "TAG_MainActivity";



    /* ***********************************************
     *************** FIELDS **************************
     * ********************************************* */
    private MainActivity mContext;
    private Fragment mShowingFragment;
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;



     /* ***********************************************
     *************** METHODS *************************
     * ********************************************* */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        initViews();

        Log.d(TAG,"Init");


        if (savedInstanceState == null) {
            mShowingFragment = ShowingFragment.newInstance();
            mFragmentManager = getSupportFragmentManager();
            mFragmentTransaction = mFragmentManager.beginTransaction();
            mFragmentTransaction.replace(R.id.container,mShowingFragment).commit();

        }else{

        }
    }

    private void initViews() {
        // inflate and injects views
        ButterKnife.inject(this);


    }


    /*Sra Yasmina final pasillo VIP
            copia cedula y rif y constancia de trabajo
            12
    21100 bs
    */


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
