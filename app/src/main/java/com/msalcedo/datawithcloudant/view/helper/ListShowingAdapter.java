package com.msalcedo.datawithcloudant.view.helper;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.msalcedo.datawithcloudant.R;
import com.msalcedo.datawithcloudant.model.database.Task;

import java.util.ArrayList;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by msalcedo on 09/02/15.
 */
public class ListShowingAdapter extends BaseAdapter {

    /** */
    private LayoutInflater mInflater;

    /** */
    private ArrayList<Task> items;

    private OnClickShowingListener mCallback;

    /** */
    private static final String TAG = "TAG_ListShowingAdapter";
    private Activity mActivity;

    /**
     *
     * @param activity
     */
    public ListShowingAdapter(Activity activity, ArrayList<Task> items, OnClickShowingListener callback) {


        // instance layout inflater
        mInflater = LayoutInflater.from(activity);
        mActivity = activity;

        this.items = items;
        mCallback = callback;


    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {

        ViewHolder holder;
        Task item =  items.get(i);

        if (view != null) {

            holder = (ViewHolder)view.getTag();


        }else {
            view = mInflater.inflate(R.layout.item_list_showing, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }


        //holder load





        return view;
    }





    @Override
    public boolean isEnabled(int position) {
        return false;
    }

    public void replace(Activity context, ArrayList<Task> items) {


        // instance layout inflater

        mActivity = context;


        this.items.clear();
        this.items.addAll(items);

        mInflater = LayoutInflater.from(mActivity);

        notifyDataSetChanged();


    }


    public void add(Task t) {
        items.add(t);
        notifyDataSetChanged();
    }

    public void set(int position, Task t) {
        items.set(position, t);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        items.remove(position);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    class ViewHolder {

        public int num;





        public ViewHolder(View view) {

             ButterKnife.inject(this, view);

        }

    }

    public interface OnClickShowingListener {
        void onClick();

    }

}