package com.msalcedo.datawithcloudant.model.base;

import java.util.List;

/**
 * Created by msalcedo on 09/02/15.
 */
public interface IShowingModel {

    public void setCallbacks(IShowingModelCallbacks callbacks);
    void getItems();
    void addItem(String str);



    interface IShowingModelCallbacks{
        void onItemsSuccess(List<String> items);
        void onItemsFailed();

        void onFailedConnection();
    }
}
