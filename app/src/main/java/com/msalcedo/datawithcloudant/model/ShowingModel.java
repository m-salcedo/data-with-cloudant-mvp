package com.msalcedo.datawithcloudant.model;

import android.content.Context;
import android.util.Log;

import com.msalcedo.datawithcloudant.model.base.IShowingModel;

/**
 * Created by msalcedo on 09/02/15.
 */
public class ShowingModel implements IShowingModel {

    private final static String TAG = "TAG_ShowingModel";


    private Context mContext;
    private IShowingModelCallbacks mCallbacks;



    public ShowingModel(Context context){
        mContext = context;
        Log.d(TAG, "Init");


    }

    @Override
    public void setCallbacks(IShowingModelCallbacks callbacks) {
        mCallbacks = callbacks;

    }

    @Override
    public void getItems() {

    }

    @Override
    public void addItem(String str) {

    }
}
